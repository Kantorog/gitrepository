printf "\\file    lab1-1.rb\n\\brief   Calculate distance between 2 points\n\\author  Kharin A.A.\n\n\n"
printf "Input coordinates of the first point...\nX: "
x1 = gets.to_i
printf "Y: "
y1 = gets.to_i
printf "\nInput coordinates of the second point...\nX: "
x2 = gets.to_i
printf "Y: "
y2 = gets.to_i
dist = ((x1 - x2)**2 + (y1 - y2)**2)**0.5
printf "Distance between two points is %4.2f", dist
gets

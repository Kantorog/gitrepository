printf "\\file    lab1-2.rb\n\\brief   Showing second digit of number's tail\n\\author  Kharin A.A.\n\n\n"
printf "Input positive rational number: "
number = gets.to_f
printf "Second digit of number's tail is %d", ((number - number.floor)*100) % 10
gets

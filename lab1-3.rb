printf "\\file    lab1-3.rb\n\\brief   Calculate rectangle's perimeter, square and capacity\n\\author  Kharin A.A.\n\n\n"
printf "Input width: "
width  = gets.to_f
printf "Input height: "
height = gets.to_f
printf "Input top: "
top    = gets.to_f
printf "\n\nPerimeter is %6.2f\nSquare is %6.2f\nCapacity is %6.2f", width * 2 + height * 2, width * height, width * height * top
gets

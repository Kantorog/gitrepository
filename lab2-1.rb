print "\\file    lab2-1.rb\n\\brief   RoR for 1 position\n\\author  Kharin A.A.\n\n\n"
a = [1, 2, 3, 4, 5, 6, 7 ,8]
print "Buff: \t"
for i in 0 .. 7
  print  a[i], "\t"
end
a.unshift(a.pop)
print "\nResult: "
for i in 0 .. 7
  print a[i], "\t"
end
gets
print "\\file    lab2-2.rb\n\\brief   Find element array least close to rational number.\n\\author  Kharin A.A.\n\n\n"
print "Enter ratinal number: "
num = gets.to_f
print "Enter array size: "
size = gets.to_i
a = Array.new(size)
printf "Enter array\n"
for i in 0 .. size - 1
  printf "a[%d]: ", i
  a[i] = gets.to_f
end
dist = (num - a[0]).abs
index = 0
print "\n\nArray: "
for i in 0 .. size - 1
  print  a[i], "\t"
  if dist < (num - a[i]).abs
    dist = (num - a[i]).abs
    index = i
  end
end

printf "\nLeast close element is %6.2f", a[index]
gets
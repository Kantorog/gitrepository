print "\\file    lab2-3.rb\n\\brief   Print the array index in that order in which corresponding elements form an increasing sequence.\n\\author  Kharin  A.A.\n\n\n"
print "Enter array size: "
size = gets.to_i
a = Array.new(size)
printf "Enter array\n"
for i in 0 .. size - 1
  printf "a[%d]: ", i
  a[i] = gets.to_i
end

print "Indexes: "
p (0...a.size).sort_by{ |i| a[i] }
gets
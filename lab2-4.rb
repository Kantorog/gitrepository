print "\\file    lab2-4.rb\n\\brief   Find the minimum odd element.\n\\author  Kharin  A.A.\n\n\n"
print "Enter array size: "
size = gets.to_i
a = Array.new(size)
printf "Enter array\n"
for i in 0 .. size - 1
  printf "a[%d]: ", i
  a[i] = gets.to_i
end

min = a[1]
for i in 0 .. size - 1
  if((i % 2) == 1 && a[i] < min)
    min = a[i]
  end
end

printf "Minimum odd element: %d", min
gets
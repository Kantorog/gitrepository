print "\\file    lab3-1.rb\n\\brief   Find the number of elements located between first and last maximum.\n\\author  Kharin  A.A.\n\n\n"
print "Enter array size: "
size = gets.to_i
a = Array.new(size)
printf "Enter array\n"
for i in 0 .. size - 1
  printf "a[%d]: ", i
  a[i] = gets.to_i
end


printf "Number of elements located between first and last maximum: %d", size - (a.reverse.index(a.max) + a.index(a.max) + 2)
gets
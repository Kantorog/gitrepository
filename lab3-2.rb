print "\\file    lab3-2.rb\n\\brief   Check whether it alternated positive and negative numbers.\n\\author  Kharin  A.A.\n\n\n"
print "Enter array size: "
size = gets.to_i
a = Array.new(size)
printf "Enter array\n"
for i in 0 .. size - 1
  printf "a[%d]: ", i
  a[i] = gets.to_i
end


printf "Is it alternated positive and negative numbers? "
p (0..a.size-2).all?{ |x| (a[x] * a[x+1]) < 0 }
gets
print "\\file    lab3-3.rb\n\\brief   Find the product of digits.\n\\author  Kharin  A.A.\n\n\n"
print "Enter number: "
number = gets.to_i

mul = 1
while number > 0
	mul = mul * (number % 10)
	number = (number / 10).floor
end

printf "The product of digits is %d", mul
gets
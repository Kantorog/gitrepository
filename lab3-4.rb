print "\\file    lab3-4.rb\n\\brief   Find the arithmetic mean of the array.\n\\author  Kharin  A.A.\n\n\n"
print "Enter array size: "
size = gets.to_i
a = Array.new(size)
printf "Enter array\n"
sum = 0
for i in 0 .. size - 1
  printf "a[%d]: ", i
  a[i] = gets.to_i
  sum += a[i]
end


puts "The arithmetic mean of the array is #{sum / (size * 1.0)}"
gets
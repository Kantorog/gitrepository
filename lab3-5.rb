print "\\file    lab3-5.rb\n\\brief   Given integer array and number K. Print index of the first element, more then K.\n\\author  Kharin  A.A.\n\n\n"
print "Enter array size: "
size = gets.to_i
a = Array.new(size)
printf "Enter array\n"
sum = 0
for i in 0 .. size - 1
  printf "a[%d]: ", i
  a[i] = gets.to_i
  sum += a[i]
end

print "Enter number: "
number = gets.to_i

i = 0
while(a[i] <= number)
  i = i + 1
end

printf "Index of the first element, more then K is %d", i
gets